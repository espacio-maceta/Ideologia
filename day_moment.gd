extends Node2D

var rng = RandomNumberGenerator.new()


enum { MORNING, AFTERNOON, NIGHT }

var current_period = MORNING
var current_event

func _process(delta):
	if Input.is_action_just_pressed('ui_select'):
		next_period()
	

func next_period():
	current_period += 1
	if current_period > NIGHT:
		current_period = MORNING
	print(current_period)
	update_sprite()
	pick_happening()

func pick_happening():
	var events = [
		{ 'background': 'Background.png', 'text': 'Bienvenido a la ofi' },
		{ 'background': 'CantinaBackground.png', 'text': 'Bienvenido a la cantina' }
	]
	var event = events[rng.randi_range(0, events.size() - 1)]
	$Event.start(event)

func update_sprite():
	if current_period == MORNING:
		$MorningSprite.show()
		$AfternoonSprite.hide()
		$NightSprite.hide()
	if current_period == AFTERNOON:
		$MorningSprite.hide()
		$AfternoonSprite.show()
		$NightSprite.hide()
	if current_period == NIGHT:
		$MorningSprite.hide()
		$AfternoonSprite.hide()
		$NightSprite.show()
