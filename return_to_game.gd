extends Button

func _on_return_to_game_pressed():
	self.queue_free()
	get_tree().change_scene("res://game.tscn")
